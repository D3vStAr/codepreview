<?php get_header(); ?>
<section class="container">
    <h3>Fonts</h3>
    <div class="row">
        <div class="col-xs-12 col-md-3 boiler-left">
            <p><strong>Joanna Sans Nova Bold</strong>,<br /> Font-Size:16px, Line-Height:24px, Font-Weight: 700</p>
        </div>
        <div class="col-xs-12 col-md-3 text-center boiler-center">
            <strong>Heading Caption</strong>
        </div>
        <div class="col-xs-12 col-md-6 boiler-right">
            <div class="heading-caption">Ephemeris</div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-3 boiler-left">
            <p><strong>Joanna Nova Book</strong>,<br /> Font-Size:44px, Line-Height:48px, Font-Weight:400</p>
        </div>
        <div class="col-xs-12 col-md-3 text-center boiler-center">
            <strong>H1</strong>
        </div>
        <div class="col-xs-12 col-md-6 boiler-right">
            <h1>How the Ephemeris Career Path Works</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-3 boiler-left">
            <p><strong>Joanna Sans Nova Regular</strong>,<br /> Font-Size:14px, Line-Height:20px, Font-Weight:300
            </p>
        </div>
        <div class="col-xs-12 col-md-3 text-center boiler-center">
            <strong>Heading Body Copy</strong>
        </div>
        <div class="col-xs-12 col-md-6 boiler-right heading-body">
            <p>Below are the possible routes and courses to take when you decide you want to start in the industry.
                This is a comprehensive and succinct guide.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-3 boiler-left">
            <p><strong>Joanna Nova Book</strong>,<br /> Font-Size:44p, Line-Height:24px, Font-Weight: 400</p>
        </div>
        <div class="col-xs-12 col-md-3 text-center boiler-center">
            <strong>H3</strong>
        </div>
        <div class="col-xs-12 col-md-6 boiler-right">
            <h3>Top Rated Schools</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-3 boiler-left">
            <p><strong>Joanna Sans Nova Regular</strong>,<br />Font-Size:14px, Line-Height:20px, Font-Weight: 300
            </p>
        </div>
        <div class="col-xs-12 col-md-3 text-center boiler-center">
            <strong>Body Copy</strong>
        </div>
        <div class="col-xs-12 col-md-6 boiler-right">
            <p>Below are the possible routes and courses to take when you decide you want to start in the industry.
                This is a comprehensive and succinct guide.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-3 boiler-left">
            <p><strong>Joanna Sans Nova Regular</strong>,<br />Font-Size:11px, Line-Height:20px, Font-Weight: 300
            </p>
        </div>
        <div class="col-xs-12 col-md-3 text-center boiler-center">
            <strong>Small Body Copy</strong>
        </div>
        <div class="col-xs-12 col-md-6 boiler-right">
            <p><small>Below are the possible routes and courses to take when you decide you want to start in the industry.
                    This is a comprehensive and succinct guide.</small></p>
        </div>
    </div>
</section>
<section class="container">
    <h3>Small Buttons</h3>
    <div class="row">
        <div class="  col-xs-12 col-md-3">
            <?php $flex->builder->html->renderButton('View More', array('class' => 'button button--dark')); ?>
        </div>
        <div class="  col-xs-12 col-md-3">
            <?php $flex->builder->html->renderButton('View More', array('class' => 'button button--blue')); ?>
        </div>
        <div class="  col-xs-12 col-md-3">
            <?php $flex->builder->html->renderButton('View More', array('class' => 'button')); ?>
        </div>

    </div>
</section>
<section class="container">
    <h3>Large Buttons</h3>
    <div class="row">
        <div class="  col-xs-12 col-md-3">
            <?php $flex->builder->html->renderButton('View More', array('class' => 'button button--dark button--large')); ?>
        </div>
        <div class="  col-xs-12 col-md-3">
            <?php $flex->builder->html->renderButton('View More', array('class' => 'button button--blue button--large')); ?>
        </div>
        <div class="  col-xs-12 col-md-3">
            <?php $flex->builder->html->renderButton('View More', array('class' => 'button button--large')); ?>
        </div>

    </div>
</section>
<section class="container">
    <h3>Career Path Internal Buttons</h3>
    <div class="row">

        <div class="  col-xs-12 col-md-6">
            <?php $flex->builder->html->renderPathButton('Where Do I Do This?', array('class' => 'path-button'), 'seg-icon-search'); ?>
        </div>
        <div class="  col-xs-12 col-md-6">
            <?php $flex->builder->html->renderPathButton('Download Questionnaire', array('class' => 'path-button'), 'seg-Checklist-Icon'); ?>
        </div>
    </div>
</section>
<?php get_footer();?>