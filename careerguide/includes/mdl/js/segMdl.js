﻿var segMdl = {
    validator: {
        validate: function (inputObj, mdlElement) {
            if (!$(inputObj)[0].hasAttribute("data-mdlvalidation")) {
                return true;
            }
            if ($(inputObj)[0].hasAttribute("disabled")) {
                return true;
            }
            if ($(inputObj).data("mdlvalidation") === "") return true;

            var _val = $(inputObj).val();
            var validations = $(inputObj).data("mdlvalidation").split(",");
            var isValid = true;
            for (var i = 0, len = validations.length; i < len; i++) {
                if (!this.validationMethods[validations[i].trim()](_val)) {
                    isValid = false;
                }
            }
            if (mdlElement != null) {
                mdlElement.errorHandler(inputObj, isValid);
            }
            return isValid;
        },
        validationMethods: {
            notempty: function (val) {
                return (val != "");
            },
            email: function (val) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(val);
            },
            date: function (val) {
                var d = new Date(val);

                if (Object.prototype.toString.call(d) === "[object Date]") {
                    // it is a date
                    if (isNaN(d.getTime())) {  // d.valueOf() could also work
                        // date is not valid
                        return false;
                    } else {
                        // date is valid
                        return true;
                    }
                } else {
                    // not a date
                    return false;
                }
            }
        },
        validateElements: function (container) {
            var isValid = true;
            for (var elementName in segMdl.elements) {
                try {
                    var element = segMdl.elements[elementName];
                    $(container)
                        .find(element.selector)
                        .each(function (index, elObj) {
                            //special case for upload fields
                            if (elementName == 'uploadNew' || elementName == 'uploadSaved') {
                                var uploadValid = segMdl.validator.validate(elObj, element);
                                var uploadOtherValid = segMdl.validator
                                    .validate($(elObj).closest('.popup-upload-component').find(element.selectorOther),
                                        element);
                                if (!uploadValid && !uploadOtherValid) {
                                    isValid = false;
                                }
                            } else {
                                if (!segMdl.validator.validate(elObj, element)) {
                                    isValid = false;
                                }
                            }
                        });
                } catch (err) {
                    console.log('validation error: ' + elementName);
                    console.log(err);
                    isValid = false;
                }
            }
            return isValid;
        }
    },
    elements: {
        searchInput: {
            selector: ".seg-mdl-searchfield #search-input",
            events: {
                focus: function () {
                    var parentObj = $(this).parent(".seg-mdl-searchfield");
                    if (!parentObj.hasClass("seg-mdl-completed")) {
                        parentObj.addClass("seg-mdl-completed");
                    }
                    parentObj.addClass("seg-mdl-focussed");
                },
                blur: function () {
                    var parentObj = $(this).parent(".seg-mdl-searchfield");
                    if (!$(this).val()) {
                        parentObj.removeClass("seg-mdl-completed");
                    }
                    parentObj.removeClass("seg-mdl-focussed");
                },
                change: function () {
                    if (!$(this).is(":focus")) { // the browser is changing stuff...
                        if ($(this).val()) {
                            $(this).parent(".seg-mdl-searchfield").addClass("seg-mdl-completed");
                        }
                    }
                }
            },
            init: function (elObj) {
                var parentObj = $(elObj).parent(".seg-mdl-searchfield");
                if ($(elObj).val()) {
                    parentObj.addClass("seg-mdl-completed");
                }
            }
        },
        textInput: {
            selector: ".seg-mdl-textfield input",
            events: {
                focus: function () {
                    var parentObj = $(this).parent(".seg-mdl-textfield");
                    if (!parentObj.hasClass("seg-mdl-completed")) {
                        parentObj.addClass("seg-mdl-completed");
                    }
                    parentObj.addClass("seg-mdl-focussed");
                },
                blur: function () {
                    var parentObj = $(this).parent(".seg-mdl-textfield");
                    if (!$(this).val()) {
                        parentObj.removeClass("seg-mdl-completed");
                    }
                    parentObj.removeClass("seg-mdl-focussed");
                    segMdl.validator.validate(this, segMdl.elements.textInput);
                },
                change: function () {
                    if (!$(this).is(":focus")) { // the browser is changing stuff...
                        if ($(this).val()) {
                            $(this).parent(".seg-mdl-textfield").addClass("seg-mdl-completed");
                        }
                    }
                    segMdl.validator.validate(this, segMdl.elements.textInput)
                }
            },
            init: function (elObj) {
                var parentObj = $(elObj).parent(".seg-mdl-textfield");
                if ($(elObj).val()) {
                    parentObj.addClass("seg-mdl-completed");
                }

                if ($(elObj).hasClass("input-validation-error")) {
                    this.errorHandler(elObj, false);
                    parentObj.addClass("seg-mdl-focussed");
                }
            },
            errorHandler: function (inputObj, isValid) {
                var parentObj = $(inputObj).parent(".seg-mdl-textfield");
                if (isValid) {
                    parentObj.removeClass("mdl-validation-error").removeClass("seg-mdl-error").children(".mdl-validation-error-msg").remove();
                } else {
                    if (!parentObj.hasClass("mdl-validation-error")) {
                        parentObj.addClass("mdl-validation-error").append("<div class=\"mdl-validation-error-msg\">" + $(inputObj).attr("data-val-required") + "</div>");
                    }
                }
            }

        },
        signaturePad: {
            selector: ".seg-mdl-signature input",
            events: {
                //focus: function () {
                //    var parentObj = $(this).parent(".seg-mdl-signature");
                //    if (!parentObj.hasClass("seg-mdl-completed")) {
                //        parentObj.addClass("seg-mdl-completed");
                //    }
                //    parentObj.addClass("seg-mdl-focussed");
                //},
                //blur: function () {
                //    var parentObj = $(this).parent(".seg-mdl-signature");
                //    if (!$(this).val()) {
                //        parentObj.removeClass("seg-mdl-completed");
                //    }
                //    parentObj.removeClass("seg-mdl-focussed");
                //    segMdl.validator.validate(this, segMdl.elements.textInput);
                //},
                change: function () {
                    //if (!$(this).is(":focus")) { // the browser is changing stuff...
                    if ($(this).val()) {
                        $(this).closest(".seg-mdl-signature").addClass("seg-mdl-completed");
                    }
                    //}
                    segMdl.validator.validate(this, segMdl.elements.signaturePad);
                }
            },
            init: function (elObj) {
                var parentObj = $(elObj).closest(".seg-mdl-signature");
                if ($(elObj).val()) {
                    parentObj.addClass("seg-mdl-completed");
                }

                if ($(elObj).hasClass("input-validation-error")) {
                    this.errorHandler(elObj, false);
                    parentObj.addClass("seg-mdl-focussed");
                }

                parentObj.find(".signatureCanvas")
                    .click(function () {
                        console.log("canvas click");
                        $(elObj).trigger("change");
                    });
            },
            errorHandler: function (inputObj, isValid) {
                var parentObj = $(inputObj).closest(".seg-mdl-signature");
                if (isValid) {
                    parentObj.removeClass("mdl-validation-error").children(".mdl-validation-error-msg").remove();
                } else {
                    if (!parentObj.hasClass("mdl-validation-error")) {
                        parentObj.addClass("mdl-validation-error").append("<div class=\"mdl-validation-error-msg\">" + $(inputObj).attr("data-val-required") + "</div>");
                    }
                }
            }

        },
        textInputFieldList: {
            selector: ".seg-mdl-textfieldlist .seg-mdl-tfl-input-section input",
            events: {
                focus: function () {
                    var parentObj = $(this).closest(".seg-mdl-textfieldlist");
                    if (!parentObj.hasClass("seg-mdl-completed")) {
                        parentObj.addClass("seg-mdl-completed");
                    }
                    parentObj.addClass("seg-mdl-focussed");
                },
                blur: function () {
                    var parentObj = $(this).closest(".seg-mdl-textfieldlist");
                    if (!$(this).val()) {
                        parentObj.removeClass("seg-mdl-completed");
                    }
                    parentObj.removeClass("seg-mdl-focussed");
                    segMdl.validator.validate(this, segMdl.elements.textInput);
                },
                change: function () {
                    var $this = $(this);
                    if (!$this.is(":focus")) { // the browser is changing stuff...
                        if ($this.val()) {
                            $this.closest(".seg-mdl-textfieldlist").addClass("seg-mdl-completed");
                        }
                    }
                    //segMdl.elements.textInputFieldList.list.filter($this);
                    segMdl.validator.validate(this, segMdl.elements.textInput);
                },
                keyup: function () {
                    segMdl.elements.textInputFieldList.list.filter($(this));
                    segMdl.elements.textInputFieldList.list.toggle($(this), 'show');
                }
            },
            init: function (elObj) {
                var parentObj = $(elObj).closest(".seg-mdl-textfieldlist");
                if ($(elObj).val()) {
                    parentObj.addClass("seg-mdl-completed");
                }

                if ($(elObj).hasClass("input-validation-error")) {
                    this.errorHandler(elObj, false);
                    parentObj.addClass("seg-mdl-focussed");
                }

                //bind dropdown btn
                parentObj.find('.seg-mdl-tfl-btn').unbind('click', segMdl.elements.textInputFieldList.list.toggle);
                parentObj.find('.seg-mdl-tfl-btn').bind('click', segMdl.elements.textInputFieldList.list.toggle);

                segMdl.elements.textInputFieldList.list.filter($(elObj));
            },
            list: {
                filter: function (inputObj) {
                    var id = inputObj.attr("id");
                    var filterString = inputObj.val().toUpperCase();
                    var parentObj = inputObj.closest(".seg-mdl-textfieldlist");
                    var respHtml = '';
                    var options = window[id + '_fieldlist_vals'];

                    for (var i = 0, len = options.length; i < len; i++) {
                        if (options[i].indexOf(filterString) !== -1) {
                            if (options[i] == filterString) {
                                respHtml += '<div class="seg-mdl-tfl-ls-item seg-mdl-selected" onclick="javascript:segMdl.elements.textInputFieldList.list.click(this);">' + options[i] + '</div>';
                                //change to matched value if case is different
                                inputObj.val(filterString);
                            } else {
                                respHtml += '<div class="seg-mdl-tfl-ls-item" onclick="javascript:segMdl.elements.textInputFieldList.list.click(this);">' + options[i] + '</div>';
                            }
                        }
                    }
                    parentObj.children('.seg-mdl-tfl-list-section').html(respHtml);
                },
                click: function (e) {
                    var $this = $(e);
                    var parentObj = $this.closest('.seg-mdl-textfieldlist');
                    parentObj.find('.seg-mdl-tfl-ls-item').removeClass('seg-mdl-selected');
                    $this.addClass('seg-mdl-selected');
                    parentObj.find('input').val($this.html());
                },
                toggle: function (inputObj, force) {

                    console.log(this.filter);

                    if (this.filter != null) {
                        var listObj = $(inputObj).closest('.seg-mdl-textfieldlist').children('.seg-mdl-tfl-list-section');
                    } else { //btn
                        return;
                        var listObj = $(this).closest('.seg-mdl-textfieldlist').children('.seg-mdl-tfl-list-section');
                    }

                    //console.log(listObj);
                    if (force != null) {
                        if (force == 'show') {
                            listObj.addClass('seg-mdl-open');
                        } else {
                            listObj.removeClass('seg-mdl-open');
                        }
                    } else {
                        if (listObj.hasClass('seg-mdl-open')) {
                            listObj.removeClass('seg-mdl-open');
                        } else {
                            listObj.addClass('seg-mdl-open');
                        }
                    }
                    console.log($(this).html());
                }
            },
            errorHandler: function (inputObj, isValid) {
                var parentObj = $(inputObj).closest(".seg-mdl-textfieldlist");
                if (isValid) {
                    parentObj.removeClass("mdl-validation-error").children(".mdl-validation-error-msg").remove();
                } else {
                    if (!parentObj.hasClass("mdl-validation-error")) {
                        parentObj.addClass("mdl-validation-error").append("<div class=\"mdl-validation-error-msg\">" + $(inputObj).attr("data-val-required") + "</div>");
                    }
                }
            }

        },
        passwordInput: {
            selector: ".seg-mdl-passwordfield input",
            events: {
                focus: function () {
                    var parentObj = $(this).parent(".seg-mdl-passwordfield");
                    if (!parentObj.hasClass("seg-mdl-completed")) {
                        parentObj.addClass("seg-mdl-completed");
                    }
                    parentObj.addClass("seg-mdl-focussed");
                },
                blur: function () {
                    var parentObj = $(this).parent(".seg-mdl-passwordfield");
                    if (!$(this).val()) {
                        parentObj.removeClass("seg-mdl-completed");
                    }
                    parentObj.removeClass("seg-mdl-focussed");
                },
                change: function () {
                    if (!$(this).is(":focus")) { // the browser is changing stuff...
                        if ($(this).val()) {
                            $(this).parent(".seg-mdl-passwordfield").addClass("seg-mdl-completed");
                        }
                    }
                }
            },
            init: function (elObj) {
                if ($(elObj).val()) {
                    $(elObj).parent(".seg-mdl-passwordfield").addClass("seg-mdl-completed");
                }
                //chrome autocomplete workaround...
                setTimeout(function () {
                    if ($(elObj).css('backgroundColor') == "rgb(250, 255, 189)") {
                        $(elObj).parent(".seg-mdl-passwordfield").addClass("seg-mdl-completed");
                    }
                }, 500);
            }
        },
        checkBox: {
            selector: ".seg-mdl-checkbox input[type='checkbox']",
            events: {
                change: function () {
                    $(this).parent(".seg-mdl-checkbox").removeClass("seg-mdl-inactive");
                    if ($(this).is(":checked")) {
                        $(this).parent(".seg-mdl-checkbox").addClass("seg-mdl-checked");
                    } else {
                        $(this).parent(".seg-mdl-checkbox").removeClass("seg-mdl-checked");
                    }
                },
            },
            init: function (elObj) {
                if ($(elObj).is(":checked")) {
                    $(elObj).parent(".seg-mdl-checkbox").removeClass("seg-mdl-inactive");
                    $(elObj).parent(".seg-mdl-checkbox").addClass("seg-mdl-checked");
                }
            }
        },
        dropDown: {
            selector: ".seg-mdl-dropdown select",
            events: {
                change: function () {
                    //if ($(this).val()) {
                    //    $(this).parent(".seg-mdl-dropdown").addClass("seg-mdl-completed");
                    //} else {
                    //    $(this).parent(".seg-mdl-dropdown").removeClass("seg-mdl-completed");
                    //}
                    segMdl.validator.validate(this, segMdl.elements.dropDown);
                },
                focus: function () {
                    var parentObj = $(this).parent(".seg-mdl-dropdown");
                    if (!parentObj.hasClass("seg-mdl-completed")) {
                        parentObj.addClass("seg-mdl-completed");
                    }
                    parentObj.addClass("seg-mdl-focussed");
                },
                blur: function () {
                    var parentObj = $(this).parent(".seg-mdl-dropdown");
                    if (!$(this).val()) {
                        parentObj.removeClass("seg-mdl-completed");
                    }
                    parentObj.removeClass("seg-mdl-focussed");
                    segMdl.validator.validate(this, segMdl.elements.dropDown);
                },
            },
            init: function (elObj) {
                if ($(elObj).val()) {
                    $(elObj).parent(".seg-mdl-dropdown").addClass("seg-mdl-completed");
                }

                if ($(elObj).hasClass("input-validation-error")) {
                    this.errorHandler(elObj, false);
                    $(elObj).parent(".seg-mdl-dropdown").addClass("seg-mdl-focussed");
                }
            },
            errorHandler: function (inputObj, isValid) {
                var parentObj = $(inputObj).parent(".seg-mdl-dropdown");
                if (isValid) {
                    parentObj.removeClass("mdl-validation-error").children(".mdl-validation-error-msg").remove();
                } else {
                    if (!parentObj.hasClass("mdl-validation-error")) {
                        parentObj.addClass("mdl-validation-error").append("<div class=\"mdl-validation-error-msg\">" + $(inputObj).attr("data-val-required") + "</div>");
                    }
                }
            }
        },
        textArea: {
            selector: ".seg-mdl-textarea textarea",
            events: {
                focus: function () {
                    var parentObj = $(this).parent(".seg-mdl-textarea");
                    if (!parentObj.hasClass("seg-mdl-completed")) {
                        parentObj.addClass("seg-mdl-completed");
                    }
                    parentObj.addClass("seg-mdl-focussed");
                },
                blur: function () {
                    var parentObj = $(this).parent(".seg-mdl-textarea");
                    if (!$(this).val()) {
                        parentObj.removeClass("seg-mdl-completed");
                    }
                    parentObj.removeClass("seg-mdl-focussed");
                    segMdl.validator.validate(this, segMdl.elements.textArea);
                },
            },
            init: function (elObj) {

                var parentObj = $(elObj).parent(".seg-mdl-textarea");
                if ($(elObj).val()) {
                    parentObj.addClass("seg-mdl-completed");
                }

                if ($(elObj).hasClass("input-validation-error")) {
                    this.errorHandler(elObj, false);
                    parentObj.addClass("seg-mdl-focussed");
                }
            },
            errorHandler: function (inputObj, isValid) {
                var parentObj = $(inputObj).parent(".seg-mdl-textarea");
                if (isValid) {
                    parentObj.removeClass("mdl-validation-error").removeClass("seg-mdl-error").children(".mdl-validation-error-msg").remove();
                } else {
                    if (!parentObj.hasClass("mdl-validation-error")) {
                        parentObj.addClass("mdl-validation-error").append("<div class=\"mdl-validation-error-msg\">" + $(inputObj).attr("data-val-required") + "</div>");
                    }
                }
            }

        },
        submitBtn: {
            selector: ".seg-mdl-submit-btn input",
            events: {
                mousedown: function (e) {
                    var container = $(this).parent(".seg-mdl-submit-btn");
                    var riple = container.children(".seg-mdl-riple");

                    var x = e.pageX - $(e.target).offset().left - riple.outerWidth() / 2;
                    var y = e.pageY - $(e.target).offset().top - riple.outerHeight() / 2;

                    riple.remove();
                    container.prepend('<div class="seg-mdl-riple" style="left:' + x + "px; top:" + y + 'px"></div>');
                },
                mouseup: function () {
                    $(this).parent(".seg-mdl-submit-btn").children(".seg-mdl-riple").css("opacity", "0");
                },
                mouseleave: function () {
                    $(this).parent(".seg-mdl-submit-btn").children(".seg-mdl-riple").css("opacity", "0");
                },
            }
        },
        button: {
            selector: ".seg-mdl-btn input",
            events: {
                mousedown: function (e) {
                    var container = $(this).parent(".seg-mdl-btn");
                    var riple = container.children(".seg-mdl-riple");

                    var x = e.pageX - $(e.target).offset().left - riple.outerWidth() / 2;
                    var y = e.pageY - $(e.target).offset().top - riple.outerHeight() / 2;

                    riple.remove();
                    container.prepend('<div class="seg-mdl-riple" style="left:' + x + "px; top:" + y + 'px"></div>');
                    container.addClass("seg-mdl-active");
                },
                mouseup: function () {
                    $(this).parent(".seg-mdl-btn").children(".seg-mdl-riple").css("opacity", "0");
                    $(this).parent(".seg-mdl-btn").removeClass("seg-mdl-active");
                },
                mouseleave: function () {
                    $(this).parent('.seg-mdl-btn').children('.seg-mdl-riple').css('opacity', '0');
                    $(this).parent(".seg-mdl-btn").removeClass("seg-mdl-active");
                },
            }
        },
        radioGroupA: {
            selector: ".seg-mdl-radio-group input",
            events: {
                change: function (e) {
                    var targetObj = $(this).closest(".seg-mdl-radio-group").find("input:checked").parent("label");
                    $(this).closest(".seg-mdl-radio-group").children("label").removeClass("seg-mdl-checked");
                    targetObj.addClass("seg-mdl-checked");

                    var x = targetObj.offset().left - $(this).closest(".seg-mdl-radio-group").offset().left;
// ReSharper disable once CoercedEqualsUsing
                    x = (x != 0) ? x + 1 : x;
                    var width = targetObj.outerWidth();
                    width = (x != 0) ? width - 1 : width;
                    $(this).closest(".seg-mdl-radio-group").children(".seg-mdl-radio-group-underline").css({
                        left: x + "px",
                        width: width + "px"
                    });
                }
            },
            init: function (elObj) { //gonna need some tweaking...
                var checkedObj = $(elObj).closest(".seg-mdl-radio-group").find("input:checked");
                if (checkedObj.length) {
                    checkedObj.trigger("change");
                }
            }
        },
        radioGroupB: {
            selector: ".seg-mdl-radio-group label span",
            events: {
                mousedown: function (e) {
                    var container = $(this).parent("label");
                    var riple = container.children(".seg-mdl-riple");

                    var x = e.pageX - $(e.target).offset().left - riple.outerWidth() / 2;
                    var y = e.pageY - $(e.target).offset().top - riple.outerHeight() / 2;

                    riple.remove();
                    container.prepend('<div class="seg-mdl-riple" style="left:' + x + "px; top:" + y + 'px"></div>');
                },
                mouseup: function () {
                    $(this).parent("label").children(".seg-mdl-riple").css("opacity", "0");
                },
                mouseleave: function () {
                    $(this).parent("label").children(".seg-mdl-riple").css("opacity", "0");
                }
            }
        },
        radioBtns: {

            selector: ".seg-mdl-radio input",

            events: {

                change: function (e) {

                    $('.' + $(this).attr('class')).parent('.seg-mdl-radio').removeClass("seg-mdl-checked");

                    $('.' + $(this).attr('class') + ':checked').parent('.seg-mdl-radio').addClass("seg-mdl-checked");

                },

                click: function (e) {

                    $('.' + $(this).attr('class')).parent('.seg-mdl-radio').removeClass("seg-mdl-checked");

                    $('.' + $(this).attr('class') + ':checked').parent('.seg-mdl-radio').addClass("seg-mdl-checked");

                }

            },

            init: function (elObj) {

                var checkedObj = $(elObj).closest(".seg-mdl-radio").find("input:checked");

                if (checkedObj.length) {

                    checkedObj.trigger("change");

                }

            }

        },

        checkBoxDot: {

            selector: ".seg-mdl-dot-checkbox input",

            events: {

                change: function () {

                    $(this).parent(".seg-mdl-dot-checkbox").removeClass("seg-mdl-inactive");

                    if ($(this).is(":checked")) {

                        $(this).parent(".seg-mdl-dot-checkbox").addClass("seg-mdl-checked");

                    } else {

                        $(this).parent(".seg-mdl-dot-checkbox").removeClass("seg-mdl-checked");

                    }

                },

            },

            init: function (elObj) {

                if ($(elObj).is(":checked")) {

                    $(elObj).parent(".seg-mdl-dot-checkbox").removeClass("seg-mdl-inactive");

                    $(elObj).parent(".seg-mdl-dot-checkbox").addClass("seg-mdl-checked");

                }

            }

        },
        toolTip: {
            selector: ".seg-mdl-tooltip",
            events: {
                mouseenter: function (e) {
                    var elTt = $("#tooltip-" + $(this).attr("id"));

                    var elCustomScrollOffsetY = 0;
                    //ToDo Chat to Ryno
                    //if ($(this).parents(".mCustomScrollbar").length) {
                    //  elCustomScrollOffsetY = $(this).closest(".mCustomScrollbar").offset().top;
                    //I do not like this.
                    //if (elCustomScrollOffsetY === 88)
                    //  elCustomScrollOffsetY = 0;
                    // }

                    var elX = $(this).offset().left - $(window).scrollLeft() - elTt.outerWidth() / 2 + $(this).outerWidth() / 2;
                    var elY = $(this).offset().top - $(window).scrollTop() + $(this).outerHeight() - elCustomScrollOffsetY;
                    elTt.css({'left': elX + "px", 'top': elY + "px", 'transform': "scale(1)"});
                },
                mouseleave: function () {
                    var elTt = $("#tooltip-" + $(this).attr("id"));
                    elTt.css({'left': "-5000px", 'top': "-5000px", 'transform': "scale(0)"});
                }
            },
            init: function (elObj) {
                $(elObj).removeAttr("title");
            }
        },
        uploadNew: {
            selector: ".popup-upload-component .uploadedDocId input",
            selectorOther: ".currentDocId input",
            events: {
                change: function () {
                    segMdl.validator.validate(this, segMdl.elements.uploadNew);
                },
            },
            init: function (elObj) {
                var parentObj = $(elObj).closest(".popup-upload-component");
                if ($(elObj).hasClass("input-validation-error")) {
                    this.errorHandler(elObj, false);
                }
            },
            errorHandler: function (inputObj, isValid) {
                var parentObj = $(inputObj).closest(".popup-upload-component");
                var isValidOther = segMdl.validator.validate($(parentObj).find(this.selectorOther));
                if (isValid || isValidOther) {
                    parentObj.removeClass("mdl-validation-error").children(".mdl-validation-error-msg").remove();
                } else {
                    if (!parentObj.hasClass("mdl-validation-error")) {
                        parentObj.addClass("mdl-validation-error").append("<div class=\"mdl-validation-error-msg\">" + $(inputObj).attr("data-val-required") + "</div>");
                    }
                }
            }
        },
        uploadSaved: {
            selector: ".popup-upload-component .currentDocId input",
            selectorOther: ".uploadedDocId input",
            events: {
                change: function () {
                    segMdl.validator.validate(this, segMdl.elements.uploadSaved);
                },
            },
            init: function (elObj) {
                var parentObj = $(elObj).closest(".popup-upload-component");
                if ($(elObj).hasClass("input-validation-error")) {
                    this.errorHandler(elObj, false);
                }
            },
            errorHandler: function (inputObj, isValid) {
                var parentObj = $(inputObj).closest(".popup-upload-component");
                //first check other element...
                var isValidOther = segMdl.validator.validate($(parentObj).find(this.selectorOther));
                if (isValid || isValidOther) {
                    parentObj.removeClass("mdl-validation-error").children(".mdl-validation-error-msg").remove();
                } else {
                    if (!parentObj.hasClass("mdl-validation-error")) {
                        parentObj.addClass("mdl-validation-error").append("<div class=\"mdl-validation-error-msg\">" + $(inputObj).attr("data-val-required") + "</div>");
                    }
                }
            }
        },
        stepper: {
            selector: '.seg-mdl-counter-box input',
            events: {},
            init: function (element) {
                var $element = $(element);
                var counterBox = $element.closest(".seg-mdl-counter-box");

                // Check if we have already bound the element, if so break out early so we don't create duplicate bindings
                if ($element.data("mdl-bound")) {
                    return
                }

                if ($(counterBox).hasClass("input-validation-error")) {
                    this.errorHandler(element, false);
                }

                function adjustInputValue(field, mutator) {
                    var raw = field.val();
                    var value = parseInt(raw, 10);
                    var minValue = parseInt(field.attr("min"), 0);
                    var maxValue = parseInt(field.attr("max"), 0);
                    var step = parseInt(field.attr("step")) || 1;

                    if (Number.isInteger(value)) {
                        var proposedValue = mutator(value, step);

                        if (Number.isInteger(minValue) && proposedValue < minValue) {
                            proposedValue = minValue;
                        }

                        if (Number.isInteger(maxValue) && proposedValue > maxValue) {
                            proposedValue = maxValue;
                        }

                        if (value !== proposedValue) {
                            field.val(proposedValue);
                            field.trigger('change');
                        }
                    } else { // Invalid input reset value
                        resetFieldValue($element);
                        field.trigger('change');
                    }
                }

                function resetFieldValue(field) {
                    field.val(parseInt(field.attr("min"), 0) || 0);
                }

                $element.on('changeMinMax', function (e) {
                    e.preventDefault()
                    adjustInputValue($element, function (value) {
                        return value;
                    })
                })

                $element.on('change', function () {
                    adjustInputValue($element, function (value) {
                        return value;
                    })
                })

                // Find decrement
                counterBox.find(".count-dec").on('click', function (e) {
                    e.preventDefault()
                    $element.trigger('focus');
                    adjustInputValue($element, function (value, step) {
                        return value - step;
                    })
                })

                counterBox.find(".count-inc").on('click', function (e) {
                    e.preventDefault()
                    $element.trigger('focus');
                    adjustInputValue($element, function (value, step) {
                        return value + step;
                    })
                })

                $element.data("mdl-bound", true);
                resetFieldValue($element);
            },
        }
    },
    bind: function (parentSelector) {
        var elementsParentSelector = (parentSelector) ? parentSelector + " " : ""; //if you dont want to go through everything on the page

        [].map.call(document.querySelectorAll('[anim="ripple"]'), function (el) {

            if (el.initialised == true)
                return;
            $(el).append('<div anim="rippleContainer">');

            if ($(el).css("position") === "static") { // Need to ensure the element is relative so the ripple will work
                $(el).css("position", "relative")
            }

            el.addEventListener('mousedown', function (e) {
                // console.log("mousedown event fired");
                e = e.touches ? e.touches[0] : e;
                var r = el.getBoundingClientRect();
                var d = Math.sqrt(Math.pow(r.width, 2) + Math.pow(r.height, 2)) * 2;
                var elm = el.querySelector('[anim="rippleContainer"]');

                elm.style.cssText = '--s: 0; --o: 1;';
                elm.offsetTop;
                elm.style.cssText = '--t: 1; --o: 0; --d: ' + d + '; --x:' + (e.clientX - r.left) + '; --y:' + (e.clientY - r.top) + ';';
            });
            el.initialised = true;
        });

        for (var elementName in this.elements) {
            try {
                var element = this.elements[elementName];
                for (var eventName in element.events) {
                    $(elementsParentSelector + element.selector).unbind(eventName, element.events[eventName]);
                    $(elementsParentSelector + element.selector).bind(eventName, element.events[eventName]);
                }

                if (element.init != null) {
                    $(elementsParentSelector + element.selector).each(function (index, elObj) {
                        element.init(elObj);
                    });
                }
            } catch (err) {
                console.log('error: ' + elementName);
                console.log(err);
            }
        }
    }
}

$(document).ready(function () {
    segMdl.bind();
});