<?php 


//extensions class

require "extensions.php";



function register_mdl_styles()
{
	wp_register_style('mdl-style', get_stylesheet_directory_uri() . '/includes/mdl/styles.php');
	wp_enqueue_style('mdl-style');
}
// Register style sheet.
add_action('wp_enqueue_scripts', 'register_mdl_styles');

function register_mdl_scripts()
{
	wp_register_script('mdl-script', get_stylesheet_directory_uri() . '/includes/mdl/js/segMdl.js');
	wp_enqueue_script('mdl-script');
}
// Register style sheet.
add_action('wp_enqueue_scripts', 'register_mdl_scripts');
?>