<?php
header("Content-type: text/css; charset: UTF-8");

$css = '';
$handle = '';
$file = '';
// open the "css" directory
if ($handle = opendir('css')) {
    // list directory contents
    while (false !== ($file = readdir($handle))) {
        // only grab file names
        if (is_file('css/' . $file)) {
			if (strpos($file, 'LCK') === false) {
            	$css .= file_get_contents('css/'.$file)."\n\n";
			}
        }
    }
    closedir($handle);
    echo $css;
}

?>