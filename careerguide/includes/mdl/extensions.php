<?php

class InputExtentions
{

    public function __construct()
    {
    }

    private static $_instance = null;

    public static function init()
    {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    public function SegMdlTextBox($name, $value, $label = "input...", $validation = "", $isEchoed = true)
    {
        $html = '<div class="seg-mdl-textfield">' .
            '<input autocomplete="off" id="' . $name . '" name="' . $name . '" value="' . $value . '" class="seg-mdl" type="text" ' . (!empty($validation) ? 'data-mdlvalidation="' . $validation . '" data-val-required="' . $label . ' Required"' : '') . '>' .
            '<label for="' . $name . '">' . $label . '</label>' .
            '</div>';
        if (!$isEchoed) {
            return $html;
        } else {
            echo $html;
        }
        //return $this;
    }

    public function SegMdlDiveBtn($name, $item_id, $isEchoed = true)
    {
        $html = '<div class="seg-mdl-btn-dive">' .
            '<div class="seg-mdl-riple" style="opacity: 0;"></div>' .
            '<input type="button" name="' . str_replace(' ', '-', $name) . '" id="btn-' . str_replace(' ', '-', $name) . '" data-target="#btn-' . str_replace(' ', '-', $name) . '" value="' . $name . '" data-id="' . $item_id . '" title="' . $name . '" class="dive_item" />' .
            '</div>';
        //return $this;
        if (!$isEchoed) {
            return $html;
        } else {
            echo $isEchoed;
        }
    }


    public function SegMdlBtn($name)
    {
        echo '<div class="seg-mdl-btn">' .
            '<div class="seg-mdl-riple" style="opacity: 0;"></div>' .
            '<input type="button" name="' . str_replace(' ', '-', $name) . '" id="btn-' . str_replace(' ', '-', $name) . '" value="' . $name . '" title="' . $name . '" />' .
            '</div>';
        //return $this;
    }

    public function SegMdlBookBtn($name, $item_id)
    {
        echo '<div class="seg-mdl-btn">' .
            '<div class="seg-mdl-riple" style="opacity: 0;"></div>' .
            '<input type="button" name="' . str_replace(' ', '-', $name) . '" data-id="' . $item_id . '" id="btn-' . str_replace(' ', '-', $name) . '" value="' . $name . '" title="' . $name . '" />' .
            '</div>';
        //return $this;
    }


    public function SegMdlSubmitBtn($name, $value)
    {
        echo '<div class="seg-mdl-submit-btn">' .
            '<div class="seg-mdl-riple" style="opacity: 0;"></div>' .
            '<input type="submit" name="' . str_replace(' ', '-', $name) . '" id="btn-' . str_replace(' ', '-', $name) . '" value="' . $value . '" title="' . $name . '"/>' .
            '</div>';
        //return $this;
    }

    public function SegMdlSubmitCallBtn($name, $value)
    {
        echo '<div class="seg-mdl-submit-btn">' .
            '<div class="seg-mdl-riple" style="opacity: 0;"></div>' .
            '<input type="submit" name="' . str_replace(' ', '-', $name) . '" id="btn-' . str_replace(' ', '-', $name) . '" onClick="addInput(\'room\',\'2\')" value="' . $value . '" title="' . $name . '"/>' .
            '</div>';
        //return $this;
    }

    public function SegMdlPackageBtn($name, $value)
    {
        echo '<div class="seg-mdl-btn">' .
            '<div class="seg-mdl-riple" style="opacity: 0;"></div>' .
            '<input type="button" class="book_now" name="' . str_replace(' ', '-', $name) . '" data-id="' . $value . '" id="btn-' . str_replace(' ', '-', $name) . '" value="' . $name . '" title="' . $name . '" />' .
            '</div>';
        //return $this;
    }

    public function SegMdlCounter($name, $value, $label = "input...", $desc, $validation = "", $isEchoed = false)
    {
        $html = '<div class="seg-mdl-counter-box">' .
            '<div class="label"><label for="' . $name . '">' . $label . '</label>' .
            '<div class="desc">' . $desc . '</div></div>' .
            '<div class="inputs"><div class="count-dec" anim="ripple" data-id="' . $name . '" ></div><input autocomplete="off" id="' . $name . '" name="' . $name . '" value="' . $value . '" class="seg-mdl ' . $validation . '" type="text"  ><div class="count-inc" anim="ripple" data-id="' . $name . '"></div></div>' .
            '</div>';
        if (!$isEchoed) {
            return $html;
        } else {
            echo $html;
        }
    }


    public function SegMdlListRadio($name, $value, $label, $isCheked = false)
    {

        echo '<div class="seg-mdl-list-radio">' .

            '<input type="radio" id="' . str_replace(' ', '-', $value) . '_id" name="' . $name . '" value="' . $value . '" ' . (($isCheked) ? 'checked' : '') . '>' .

            '<label for="' . str_replace(' ', '-', $value) . '_id"><div class="seg-mdl-list-radio-dot"></div>' . $label . '</label>' .

            '</div>';

        //return $this;

    }


    public function SegMdlRadio($name, $value, $label, $isCheked = false)
    {

        echo '<div class="seg-mdl-radio">' .

            '<input type="radio" id="' . str_replace(' ', '-', $value) . '_id" class="' . str_replace(' ', '-', $name) . '_cls" name="' . $name . '" value="' . $value . '" ' . (($isCheked) ? 'checked' : '') . '>' .

            '<label for="' . str_replace(' ', '-', $value) . '_id"><div class="seg-mdl-radio-dot"></div>' . $label . '</label>' .

            '</div>';

        //return $this;

    }


    public function SegMdlDotCheckBox($name, $value, $label)
    {

        echo '<div class="seg-mdl-dot-checkbox">' .

            '<input type="checkbox" id="' . str_replace(' ', '-', $value) . '_id" name="' . $name . '" value="' . $value . '">' .

            '<label for="' . str_replace(' ', '-', $value) . '_id"><div class="seg-mdl-dot-checkbox-dot"></div>' . $label . '</label>' .

            '</div>';

        //return $this;

    }


    public function SegMdlPasswordBox($name, $value, $label)
    {

        echo '<div class="seg-mdl-passwordfield">' .

            '<input type="password" id="' . $name . '_id" name="' . $name . '" value="' . $value . '">' .

            '<label for="' . $name . '_id">' . $label . '</label>' .

            '</div>';

        //return $this;

    }


    public function SegMdlTextBoxArea($name, $value, $label, $validation = "")
    {

        echo '<div class="seg-mdl-textarea">' .

            '<textarea spellcheck="false" class="textAreaAdjust seg-mdl" name="' . $name . '"  id="' . $name . '_id" ' . (!empty($validation) ? 'data-mdlvalidation="' . $validation . '" data-val-required="' . $label . ' Required"' : '') . '>' . $value . '</textarea>' .

            '<label for="' . $name . '_id">' . $label . '</label>' .

            '</div>';

        //return $this;

    }


    public function SegMdlDropDownList($name, $value, $label, $options, $validation = "", $isEchoed = true)
    {
        $html = '<div class="seg-mdl-dropdown">' .
            '<label for="' . $name . '_id">' . $label . '</label>' .
            '<select id="' . $name . '_id" name="' . $name . '" ' . (!empty($validation) ? 'data-mdlvalidation="' . $validation . '" data-val-required="' . $label . ' Required"' : '') . '>';
        foreach ($options as $oValue => $oName) {
            $html .= '<option value="' . $oValue . '" ' . (($value == $oValue) ? 'selected' : '') . '>' . $oName . '</option>';
        }
        $html .= '</select>' .
            '<div class="seg-mdl-dropdown-arrow"></div>' .
            '</div>';

        if ($isEchoed) {
            echo $html;
        } else {
            return $html;
        }
    }


}



?>