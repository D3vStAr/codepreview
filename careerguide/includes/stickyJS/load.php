<?php




function register_sticky_styles()
{
    wp_register_style('sticky-style', get_stylesheet_directory_uri() . '/includes/stickyJS/sticky.css');
    wp_enqueue_style('sticky-style');
}
// Register style sheet.
add_action('wp_enqueue_scripts', 'register_sticky_styles');

function register_sticky_scripts()
{
    wp_register_script('sticky-script', get_stylesheet_directory_uri() . '/includes/stickyJS/sticky.js');
    wp_enqueue_script('sticky-script');
}
// Register style sheet.
add_action('wp_enqueue_scripts', 'register_sticky_scripts');