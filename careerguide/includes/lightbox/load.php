<?php




function register_lightbox_styles()
{
    wp_register_style('lightbox-style', get_stylesheet_directory_uri() . '/includes/lightbox/lightbox.css');
    wp_enqueue_style('lightbox-style');
}
// Register style sheet.
add_action('wp_enqueue_scripts', 'register_lightbox_styles');

function register_lightbox_scripts()
{
    wp_register_script('lightbox-script', get_stylesheet_directory_uri() . '/includes/lightbox/lightbox.js');
    wp_enqueue_script('lightbox-script');
}
// Register style sheet.
add_action('wp_enqueue_scripts', 'register_lightbox_scripts');


?>