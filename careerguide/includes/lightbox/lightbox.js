var lightBox = {
    topMargin: 20,

    bottomMargin: 20,

    boxWidth: 1060,

    showClose: true,

    boxClass: "",

    html:'<div class="lightbox_black_out"></div>' +
        '<div class="lightbox_container">'+

            '<div class="lightbox_content">'+

            	'<div class="lightbox_content_holder"></div>'+

				'<div class="lightbox_close"></div>'+

            	'<div class="lightbox_loader"><div class="uil-ripple-css" style="transform:scale(0.6);"><div></div><div></div></div></div>'+

            '</div>'+

        '</div>',

	init: function(selector){
        
		var self = this;

        selector = selector || "";

		this.bindEvents(selector);

	},

	positionHolder: function(){

		var contentStarts = $(document).scrollTop() + $('#header').height() + this.topMargin;

		var viewPortHeight = $(window).height() - $('#header').height() - this.topMargin - this.bottomMargin;

		

		$('.lightbox_content').css({

			//'margin-top': contentStarts+'px',

           // 'height': viewPortHeight + 'px',

           // 'max-width': this.boxWidth + 'px'

		});

    },

    isImage: function (url) {
        return (url.toLowerCase().match(/\.(jpeg|jpg|gif|png)$/) != null);
    },

	openLightBox: function(){

		$('body').append(this.html);

        $('body').addClass("displaying_modal_content")

        if (!this.showClose) {
            $('body').find(".lightbox_close").remove();
        }

        if (this.boxClass !== "") {
            $('body').find(".lightbox_content_holder").addClass(this.boxClass);
        }

		this.positionHolder();

		this.bindInternalEvents();

	},

	loadAjaxContent: function(url){

		$.ajax({

			type : "post",

			url : url+ (url.indexOf("?") >= 0 ? '&lightbox=true' : "?lightbox=true"),

			data : {

				lightbox: true

			},

			complete: function(data) {

				$('.lightbox_loader').hide();

				$('.lightbox_content_holder').html(data.responseText);

				$('.lightbox_content').css('height', 'auto');

			}

		});

	},
    loadImageContent: function (url) {
        $('.lightbox_loader').hide();
        var img = $("<img />", { src: url });
        img.on('load', function () {
            $('.lightbox_content').css('width', $(this).width());
            $('.lightbox_content').css('height', $(this).height());
        });

        $('.lightbox_content_holder').append(img);
        $('.lightbox_content').css('height', 'auto');
    },
    bindEvents: function (selector){

		var self = this;

        $(selector + ' .lightbox').click(function (event) {

            event.preventDefault();
            
            self.boxWidth = $(this).data("box-width");

            self.boxClass = $(this).data("box-class");
            
            self.showClose = $(this).data("close") != false;

            self.openLightBox();
            var url = $(this).attr('href');
            
console.log(url);



            if (self.isImage(url))
                self.loadImageContent(url);
            else
                self.loadAjaxContent(url);

			return false;

		});

		

	},

	bindInternalEvents: function(){

		var self = this;



		$('.lightbox_close').mouseenter(function(){

			$(this).addClass('lightbox_close_hover');	

		}).mouseleave(function() {

            $(this).removeClass('lightbox_close_hover');	

        }).click(function() {

            self.close();

        });

    },

    close: function () {
        $('body').removeClass("displaying_modal_content")
        $('.lightbox_container').remove();
        $('.lightbox_black_out').remove();
    }
};



$(document).ready(function() {

    lightBox.init();

});