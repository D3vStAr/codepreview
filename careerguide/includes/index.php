<?php get_header();?>
    <header class="container-fluid">
        <?php $flex->builder->html->renderBreadCrumbs('interior'); ?>
    </header>
    <div id="navbar" class="container-fluid">
        <?php $navItems = array('Deck', 'Interior', 'Engineering', 'Seminars', 'Events'); ?>
        <?php $flex->builder->html->renderNavigation($navItems, 'main'); ?>
    </div>
    <section class="container">
        <h3>Fonts</h3>
        <div class="row">
            <div class="col-xs-12 col-md-3 boiler-left">
                <p><strong>Joanna Sans Nova Bold</strong>,<br /> Font-Size:16px, Line-Height:24px, Font-Weight: 700</p>
            </div>
            <div class="col-xs-12 col-md-3 text-center boiler-center">
                <strong>Heading Caption</strong>
            </div>
            <div class="col-xs-12 col-md-6 boiler-right">
                <div class="heading-caption">Ephemeris</div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-3 boiler-left">
                <p><strong>Joanna Nova Book</strong>,<br /> Font-Size:44px, Line-Height:48px, Font-Weight:400</p>
            </div>
            <div class="col-xs-12 col-md-3 text-center boiler-center">
                <strong>H1</strong>
            </div>
            <div class="col-xs-12 col-md-6 boiler-right">
                <h1>How the Ephemeris Career Path Works</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-3 boiler-left">
                <p><strong>Joanna Sans Nova Regular</strong>,<br /> Font-Size:14px, Line-Height:20px, Font-Weight:300
                </p>
            </div>
            <div class="col-xs-12 col-md-3 text-center boiler-center">
                <strong>Heading Body Copy</strong>
            </div>
            <div class="col-xs-12 col-md-6 boiler-right heading-body">
                <p>Below are the possible routes and courses to take when you decide you want to start in the industry.
                    This is a comprehensive and succinct guide.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-3 boiler-left">
                <p><strong>Joanna Nova Book</strong>,<br /> Font-Size:44p, Line-Height:24px, Font-Weight: 400</p>
            </div>
            <div class="col-xs-12 col-md-3 text-center boiler-center">
                <strong>H3</strong>
            </div>
            <div class="col-xs-12 col-md-6 boiler-right">
                <h3>Top Rated Schools</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-3 boiler-left">
                <p><strong>Joanna Sans Nova Regular</strong>,<br />Font-Size:14px, Line-Height:20px, Font-Weight: 300
                </p>
            </div>
            <div class="col-xs-12 col-md-3 text-center boiler-center">
                <strong>Body Copy</strong>
            </div>
            <div class="col-xs-12 col-md-6 boiler-right">
                <p>Below are the possible routes and courses to take when you decide you want to start in the industry.
                    This is a comprehensive and succinct guide.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-3 boiler-left">
                <p><strong>Joanna Sans Nova Regular</strong>,<br />Font-Size:11px, Line-Height:20px, Font-Weight: 300
                </p>
            </div>
            <div class="col-xs-12 col-md-3 text-center boiler-center">
                <strong>Small Body Copy</strong>
            </div>
            <div class="col-xs-12 col-md-6 boiler-right">
                <p><small>Below are the possible routes and courses to take when you decide you want to start in the industry.
                        This is a comprehensive and succinct guide.</small></p>
            </div>
        </div>
    </section>
    <section class="container">
        <h3>Small Buttons</h3>
        <div class="row">
            <div class="  col-xs-12 col-md-3">
                <?php $flex->builder->html->renderButton('View More', array('class' => 'button button--dark')); ?>
            </div>
            <div class="  col-xs-12 col-md-3">
                <?php $flex->builder->html->renderButton('View More', array('class' => 'button button--blue')); ?>
            </div>
            <div class="  col-xs-12 col-md-3">
                <?php $flex->builder->html->renderButton('View More', array('class' => 'button')); ?>
            </div>

        </div>
    </section>
    <section class="container">
        <h3>Large Buttons</h3>
        <div class="row">
            <div class="  col-xs-12 col-md-3">
                <?php $flex->builder->html->renderButton('View More', array('class' => 'button button--dark button--large')); ?>
            </div>
            <div class="  col-xs-12 col-md-3">
                <?php $flex->builder->html->renderButton('View More', array('class' => 'button button--blue button--large')); ?>
            </div>
            <div class="  col-xs-12 col-md-3">
                <?php $flex->builder->html->renderButton('View More', array('class' => 'button button--large')); ?>
            </div>

        </div>
    </section>
    <section class="container">
        <h3>Career Path Internal Buttons</h3>
        <div class="row">

            <div class="  col-xs-12 col-md-6">
                <?php $flex->builder->html->renderPathButton('Where Do I Do This?', array('class' => 'path-button'), 'seg-icon-search'); ?>
            </div>
            <div class="  col-xs-12 col-md-6">
                <?php $flex->builder->html->renderPathButton('Download Questionnaire', array('class' => 'path-button'), 'seg-Checklist-Icon'); ?>
            </div>
        </div>
    </section>
    <footer class="container-fluid">
        <section class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="footer-inner-about">
                        <div class="footer-inner-title">About Ephemeris</div>
                        <div class="footer-inner-content">
                            <p>Possible routes and courses to take when you decide you want to start in the industry. This is a comprehensive and succinct guide.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2">
                    <div class="footer-inner-paths">
                        <div class="footer-inner-title">Career paths</div>
                        <div class="footer-inner-content">
                            <?php $navItems = array('Deck', 'Interior', 'Engineering'); ?>
                            <?php $flex->builder->html->renderNavigation($navItems, 'footer-paths'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-2">
                    <div class="footer-inner-nav">
                        <div class="footer-inner-title">Navigation</div>
                        <div class="footer-inner-content">
                            <?php $navItems = array('Seminars', 'Events', 'Terms & Conditions', 'Privacy Policy'); ?>
                            <?php $flex->builder->html->renderNavigation($navItems, 'footer-nav'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5">
                    <div class="footer-inner-newsletter">
                        <div class="footer-inner-title">Subscribe To Our Newsletter To Receive The Latest News</div>
                        <div class="footer-inner-content">
                            <?php $flex->builder->html->renderSubscribe(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php $navItems = array('Facebook' => array('class' => 'icon_facebook'), 'Twitter' => array('class' => 'icon_twitter'), 'Youtube' => array('class' => 'icon_youtube'), 'Instagram' => array('class' => 'icon_instagram')); ?>
                    <?php $flex->builder->html->renderNavigation($navItems, 'social-media'); ?>
                </div>
            </div>

            <div class="row footer-copy-notice">
                <div class="col-xs-10">
                    <p><small>Website created and managed by SEG Management Applications & Solutions</p></small>
                </div>
                <div class="col-xs-2">
                    <?php $navItems = array('PayPal' => array('class' => 'icon_payment'), 'VISA' => array('class' => 'icon_payment'), 'SKRILL' => array('class' => 'icon_payment')); ?>
                    <?php $flex->builder->html->renderNavigation($navItems, 'payment-methods'); ?>
                </div>
            </div>
        </section>
    </footer>
    <script src="assets/js/app.js"></script>
</body>

</html>