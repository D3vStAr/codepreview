<?php

/*
 * Plugin Name: SEG Custom Paths
 * Description: Career Guide by Ephemeris Path Class.
 * Version: 1.0.0
 * Author: SEG Management Solutions
 * Author URI: https://segsolutions.net
 * Text Domain: career-guide-Paths
*/


if (
    !class_exists('SEGCGPath')
) {
    class SEGCGPath
    { 
        
 
        public $postType ='Paths';
        

        function __construct()
        {   

       

            add_action('init', array($this, 'create_post_type') ,1);
            add_action( 'add_meta_boxes',array($this, 'segcg_paths_add_custom_box'),2);
            add_action('admin_enqueue_scripts',array($this, 'register_scripts'),3);
            add_action("wp_ajax_ajax_segcg_new_path_node_html",array($this, 'ajax_segcg_new_path_node_html'));
            add_action("save_post", array($this, 'save'));

           
        }

        function register_scripts()
        {
              wp_deregister_script('jquery');
              wp_register_script('jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', false, '3.5.1');
              wp_enqueue_script('jquery');


            wp_register_script('admin-dragable-module', get_stylesheet_directory_uri() . '/includes/segca-paths/modules/dragAble.js', 'jquery', '1.3.2', true);
            wp_enqueue_script('admin-dragable-module');

            wp_register_script('admin-path-script-module', get_stylesheet_directory_uri() . '/includes/segca-paths/modules/adminPath.js', 'jquery', '1.3.2', true);
            wp_enqueue_script('admin-path-script-module');

          
              
              wp_register_script('admin-path-script', get_stylesheet_directory_uri() . '/includes/segca-paths/pathScript.js' ,'jquery','1.3.2',true);
              wp_enqueue_script('admin-path-script');



              wp_add_inline_script('admin-path-script', 'const AJAXPath="' . site_url() . '/wp-admin/admin-ajax.php"', 'before');
             


              wp_register_style('style-path', get_stylesheet_directory_uri() . '/includes/segca-paths/pathStyles.css');
              wp_enqueue_style('style-path');

              
        }

       

        function create_post_type()
        {

            $name = $this->postType;
            $singular_name = 'Path';
            register_post_type(
                'segcg_' . strtolower($singular_name),
                array(
                    'labels' => array(
                        'name'               => _x($name, 'post type general name'),
                        'singular_name'      => _x($singular_name, 'post type singular name'),
                        'menu_name'          => _x($name, 'admin menu'),
                        'name_admin_bar'     => _x($singular_name, 'add new on admin bar'),
                        'add_new'            => _x('Add New', strtolower($name)),
                        'add_new_item'       => __('Add New ' . $singular_name),
                        'new_item'           => __('New ' . $singular_name),
                        'edit_item'          => __('Edit ' . $singular_name),
                        'view_item'          => __('View ' . $singular_name),
                        'all_items'          => __('All ' . $name),
                        'search_items'       => __('Search ' . $name),
                        'parent_item_colon'  => __('Parent :' . $name),
                        'not_found'          => __('No ' . strtolower($name) . ' found.'),
                        'not_found_in_trash' => __('No ' . strtolower($name) . ' found in Trash.')
                    ),
                    'description'        => __($name),

                    'public'             => true,
                    'publicly_queryable' => true,
                    'show_ui'            => true,
                    'show_in_menu'       => true,
                    'show_in_rest'     => true,
                    'query_var'          => true,
                    'rewrite'            => array('slug' => __(strtolower($singular_name))),
                    'capability_type'    => 'post',
                    'has_archive'        => true,
                    'hierarchical'       => true,
                    'menu_position'      => null,
                    'menu_icon'          => 'dashicons-schedule',
                    'supports'           => array('title', 'thumbnail'),
                    'taxonomies'          => array('category')
                )
            );
           
        }

        function segcg_paths_add_custom_box() {          
           
                add_meta_box(
                    'segcg_box_id',                 // Unique ID
                    $this->postType.' Tree Builder',      // Box title
                    [ self::class, 'segcg_path_builder_page_html' ],  // Content callback, must be of type callable
                    'segcg_' . strtolower(substr($this->postType, 0, -1))       // Post type
                );
                        
        }

        public static function save()
        {
            global $flex, $post;
            
            if (array_key_exists('new_node_field', $_POST));
            {
                update_post_meta(
                    $post->ID,
                    '_wporg_meta_key',
                    $_POST['new_node_field']
                );
            }
        }


        


 function segcg_path_builder_page_html(){

            
            global $flex,$post;

            $path_items = get_post_meta($post->ID, '_wporg_meta_key', true);              
          

           $currentCategory = get_the_category($post->ID);                 
          

            $loop = get_custom_posts('segcg_courses');
          
          
                while ( $loop->have_posts() ) : $loop->the_post();
                        $postCategory = get_the_category(get_the_ID());
                        
                        if($currentCategory[0]->slug === $postCategory[0]->slug || $currentCategory[0]->slug === $postCategory[1]->slug || $currentCategory[0]->slug === $postCategory[2]->slug){
                        
                            $courseItems[] = array('title'=> get_the_title(), 'course_id'=> get_the_ID());
                        
                        }
                endwhile;

                 wp_reset_postdata();

            $new_path_builder = new Element('div', new AttributeList(array('id' => 'path_builder_wrapper', 'class' => 'path_builder')));
            $new_course_list = new Element('div', new AttributeList(array('id' => 'course_list_wrapper','class'=>'course_list')));


            foreach($courseItems as $courseItem){
                $new_course = new Element('div', new AttributeList(array(
                    'id'=> $courseItem['course_id'],
                    'data-id' => $courseItem['course_id'],
                    'class'=>'draggable',
                    'draggable' => 'true',
                    'ondragstart' => 'onDragStart(event)',
            )));
                $new_course->addElement($courseItem['title']);
                 //$courseItem['title'];
                 $new_course_list->addElement($new_course);
            }

            $path = $flex->builder->html->pathAdminBuilder($post->ID, $path_items , array('id' => 'new_path_wrapper', 'class' => 'mew_path'));



            $new_path_builder->addElement($new_course_list);
            $new_path_builder->addElement($path);
           

            echo $new_path_builder->render();


             
            
            
           
        } 


        function ajax_segcg_new_path_node_html()
        {
            global $flex;

            $response = $_POST;
            $nodeType = $response['node_type'];

          
          
           
         if($nodeType){

                
            $flex->builder->html->newPathNodeHtml($nodeType,$response['location'], $response['post_id'] , array('id' => 'new_node_item_' . $nodeType . '_wrapper'));


                
       }else{
                $new_node_item = new Element('div', new AttributeList(array('id' => 'new_node_item_wrapper', 'location' => $response['location'], 'class' => 'new_node_item')));
                $selectNodeType = $flex->builder->html->selectNodeType($response['post_id'], array('id' => 'new_node_type_selector', 'class' => 'type_selector',));
                $new_node_item->addElement($selectNodeType);
                echo  $new_node_item->render();
       } 
             
 
           
             
      
            exit();
        }
                


        
    }

    $my_class = new SEGCGPath();
    //SEGCGPath::register_custom_settings();
}