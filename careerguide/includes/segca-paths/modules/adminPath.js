//path.somePublicVar;
//path.markerSearch();

const SEGCareerPathAdmin = function () {

    // Private vars
    var canExecute = true;
    var new_node_button = document.getElementsByClassName("new-node-button");
 

    // Public
    this.somePublicVar = null;

    // Private method
    let actualMarkerSearch = function () {

    };

    // Public method init()
    this.init = function () {
        console.log('Admin Path Object Initited')

         for (var i = 0; i < new_node_button.length; i++) {
            new_node_button[i].addEventListener('click', this.addNewNodeItemStart, false);
           
        }
       
       

    };

    
  
    // Public method
    this.addNewNodeItemStart = function () {     

        $('.new-node-button').hide();

        let location = this.dataset.location
        let post_id = this.dataset.post_id
       

        $.ajax({
            type: "post",
            url: AJAXPath,
            data: {
                action: "ajax_segcg_new_path_node_html",
                location: location,
                post_id:post_id,
           
            },
            complete: function (data, textStatus) {
                if (textStatus === "success") {

                    console.log(location)
                    $('#new-node-container-' + location).append(data.responseText)

                    const selectElement = document.querySelector('#path-node-type_id');
                    selectElement.addEventListener('change', (event) => {
                        
                        changeNodeType(event.target.value , post_id, location);
                        



                    })

                   
                      
                } else {


                    $('#new-node-container-' + location).html('Something Went Wrong Please try agian 1')
                }
            }
        });

    };

    changeNodeType = function (nodeType,post_id,location) {
      

          $.ajax({
            type: "post",
            url: AJAXPath,
            data: {
                action: "ajax_segcg_new_path_node_html",
                location: location,
                post_id:post_id,
                node_type:nodeType
            },
            complete: function (data, textStatus) {
                if (textStatus === "success") {

                    console.log(data.responseText)
                   
                 $('#new_node_item_wrapper').html(data.responseText)

                    

                   
                      
                } else {

                  
                 $('#new_node_item_wrapper').html('Something Went Wrong Please try agian 2')

                }
            }
        });



    }

};