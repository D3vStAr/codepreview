function onDragStart(event) {
    event
        .dataTransfer
        .setData('text/plain', event.target.id);

    event
        .currentTarget
        .style
        .backgroundColor = 'grey';
}


function onDragOver(event) {
    event.preventDefault();
}


function onDrop(event) {

    const id = event
        .dataTransfer
        .getData('text');

    const draggableElement = document.getElementById(id);
    
    const dropzone = event.target;

   

    dropzone.appendChild(draggableElement);

    document.getElementById("new_node_field").value = "item_id:"+ draggableElement.dataset.id +",location:"+ dropzone.dataset.location+",post_id:"+ dropzone.dataset.post_id+"";

   

    event.dataTransfer.clearData();

}