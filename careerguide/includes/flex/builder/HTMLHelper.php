<?php

class HTMLHelper
{



    public function renderNavigation($items,$position,$currentPage=null){


        $navigation = new Element('div');
        $navigation->setClass($position.'-menu-wrapper');
        $list = new Element('ul' , new AttributeList(array('id' => $position . '-menu')));
        $list->setClass('no-list');


        print_r($items);
        exit();
        foreach ($items as $key=>$item) {
            $list_item = new Element('li');

            if(!is_array($item)){
                $menu_item = new Element('a', new AttributeList(array('title'=> $item,'class' => $position . '-menu-item', 'anim' => 'ripple', 'href' => get_site_url(null, strtolower($item), $item))));
                $menu_item->setContent($item);
            }else{
                $menu_item = new Element('a', new AttributeList(array('title' => $item, 'class' => $position . '-menu-item '.$item['class'], 'anim' => 'ripple', 'href' => get_site_url(null, strtolower($key), $key))));
                $menu_item->setContent('&nbsp;');
            }

            $menu_item_html  = $menu_item->render();
            $list_item->setContent($menu_item_html);
            $list->addElement($list_item);
        }

        $navigation->setContent($list);
        if($position==='main'){
            $nav_burger = new Element('i', new AttributeList(array('class' => 'seg-menu')));       
            $navigation->addElement($nav_burger);
        }
        echo $navigation->render();

    }


     public function renderBreadCrumbs($page){

               $navItems =  wp_get_pages();

                $crumNavigation = new Element('div');
                $crumNavigation->setClass('row crum-menu-wrapper');
                $list = new Element('ul', new AttributeList(array('id' => 'crumb-menu')));
                $list->setClass('no-list');

                foreach ($navItems as $key => $item) {
                    $list_item = new Element('li');
                    $list_item->setClass('crum-list');

                   
                         if(strtolower($page) == strtolower($item)){

                            

                            $crumCurrentItem = new Element('div');
                            $crumCurrentItem->setClass('current-crumb');
                            $crumCurrentItem->setContent($item);

                        $list_item->setContent($crumCurrentItem);
                        }
                   
                     
                    $list->addElement($list_item);
                }

                $crumNavigation->setContent($list);
              
                echo $crumNavigation->render();
               

     }


    public function renderImage($src){
        $img = new Element('img', new AttributeList(array('src' => $src)));
        echo  $img->render();

    }

    public function renderLinkedImage($src,$link_url)
    {
        $img = new Element('img', new AttributeList(array('src' => $src)));
        $a = new Element('a', new AttributeList(array('href' =>$link_url, 'class' => 'lightbox')));
        $a->addElement($img);   
        echo  $a->render();
    }


    public function renderForm($formName,$fieldItems,$destination=''){

        $form = new Element('div');
        $form->setClass($formName.'-form');
        $formHeading = new Element('h3', new AttributeList(array('class'=>'form_header')));
        $formHeading->setContent($formName);
        $form_fields = new Element('div');
        foreach($fieldItems as $key=>$field){
            $form_fields->addElement(
                new MdlTextField($field, $field)
            );
        }
        $form->addElement($formHeading);
        $form->addElement($form_fields);
        echo $form->render();       

    }

    public function renderSubscribe()
    {

        $form = new Element('div');
        $form->setClass('subscribe-wrapper');
       
         $form_fields = new Element('div');
        $form_fields->setClass('input-wrapper');
            $form_fields->addElement(
                new MdlTextField('Enter your email', 'email')
            );

        $btn = new Element('button', new AttributeList());
        $btn->setClass('button button--subscribe inline_block');
        $btn->attributes->setValue('type', 'submit');        
        $btn->attributes->setValue('id', 'subscribe');
        $btn->attributes->setValue('title', 'Subscribe');
        $btn->attributes->setValue('anim', 'ripple');
        $btn->setContent('Subscribe');
      
        $form->addElement($form_fields);
        $form->addElement($btn);
        echo $form->render();
    }



    public function renderButton($label, $attr = array()){
        $btn = new Element('button', new AttributeList($attr));
        $btn->attributes->setValue('title', $label);
        $btn->attributes->setValue('anim', 'ripple');
        $btn->setContent($label);
        echo $btn->render();
    }


    public function renderPathButton($label, $attr = array(), $icon=null)
    {
        $btn = new Element('a', new AttributeList($attr));
        $btn->attributes->setValue('title', $label);
        $btn->attributes->setValue('anim', 'ripple');

        $btn_content = new Element('div', new AttributeList(array('class'=> 'button-holder')));
        $btn_content->addElement('<i class='.$icon.'></i>');
        $btn_content->addElement($label);
        $btn->setContent($btn_content);
        echo $btn->render();
    }
   

    //Path Spesific Functions

    public function pathAdminBuilder($postID, $path_items, $attr = array()){

        $nodePositions = array('top'=>12,'left'=>6,'right'=>6,'bottom'=>12);

        $path = new Element('div', new AttributeList($attr));
        $top = new Element('div', new AttributeList(array('class'=> 'long-top-holder')));
        $left = new Element('div', new AttributeList(array('class'=> 'short-left-holder')));
        $right = new Element('div', new AttributeList(array('class'=> 'short-right-holder')));
        $bottom = new Element('div', new AttributeList(array('class'=> 'long-bottom-holder')));

        if (!empty($path_items)) {
        foreach($path_items as $key=>$item){
           
            $allItems = explode(',', $item);

            foreach( $allItems as $sumItem){
                $subsubItem = explode(':', $sumItem);

                $listItems[$subsubItem[0]] = $subsubItem[1];

            }
           
            $pathItems[] =  $listItems;



        }}
       

        $path_nodes = new Row();

        foreach($nodePositions as $key=>$pos)
        {
            $$key = new Column(new ColumnDivision($pos));

           

            $node_container = new Element('div', new AttributeList(array('id'=> 'new-node-container-'.$key)));
            if (!empty($path_items)) {
            foreach($pathItems as $item){
                if ($item['location'] === $key) {
                    $node_input_item = new Element('input', new AttributeList(array('type'=>'hidden','value' => 'item_id:' . $item['item_id'] . ',location:' . $item['location'] . ',post_id:' . $item['post_id'], 'id' => 'old_node_field', 'name' => 'new_node_field[]', 'class' => 'new_node_input_field')));
                    $node_input_item_title = new Element('div',new AttributeList(array('value' => 'item_id:')));
                    $itemTitle = segma_post_title($item['item_id']);
                    $node_input_item_title->addElement($itemTitle);
                    $node_container->addElement($node_input_item);
                    $node_container->addElement($node_input_item_title);
                }
            }}
            

            $path_blank_item = new Element('div', new AttributeList(array('class'=> 'new-node-button')));
            $path_blank_item->setContent('+');            
            $path_blank_item->attributes->setValue("data-location", $key);
            $path_blank_item->attributes->setValue("data-post_id", $postID);
            $$key->addElement($node_container);
            $$key->addElement($path_blank_item);

            $path_nodes->addElement($$key);
        }
        
        

       
        $path->addElement($path_nodes);

        return $path;

    }


    public function pathFrontEndBuilder($postID, $path_items, $attr = array())
    {

        $nodePositions = array('top' => 12, 'left' => 6, 'right' => 6, 'bottom' => 12);

        $path = new Element('div', new AttributeList($attr));
        $top = new Element('div', new AttributeList(array('class' => 'long-top-holder')));
        $left = new Element('div', new AttributeList(array('class' => 'short-left-holder')));
        $right = new Element('div', new AttributeList(array('class' => 'short-right-holder')));
        $bottom = new Element('div', new AttributeList(array('class' => 'long-bottom-holder')));

        if(!empty($path_items)){

            foreach ($path_items as $key => $item) {

                $allItems = explode(',', $item);

                foreach ($allItems as $sumItem) {
                    $subsubItem = explode(':', $sumItem);

                    $listItems[$subsubItem[0]] = $subsubItem[1];
                }

                $pathItems[] =  $listItems;
            }

        }

        $path_nodes = new Row();

        foreach ($nodePositions as $key => $pos) {
            $$key = new Column(new ColumnDivision($pos));



            $node_container = new Element('div', new AttributeList(array('id' => 'node-container-' . $key)));
            if (!empty($path_items)) {
            foreach ($pathItems as $item) {
                if ($item['location'] === $key) {
                    //$node_input_item = new Element('input', new AttributeList(array('type' => 'hidden', 'value' => 'item_id:' . $item['item_id'] . ',location:' . $item['location'] . ',post_id:' . $item['post_id'], 'id' => 'old_node_field', 'name' => 'new_node_field[]', 'class' => 'new_node_input_field')));
                    $node_input_item_title = new Element('div', new AttributeList(array('class' => 'node_box')));
                    $itemTitle = segma_post_title($item['item_id']);
                    $node_input_item_title->addElement($itemTitle);
                    //$node_container->addElement($node_input_item);
                    $node_container->addElement($node_input_item_title);
                }
            }
            }


            //$path_blank_item = new Element('div', new AttributeList(array('class' => 'new-node-button')));
            //$path_blank_item->setContent('+');
            //$path_blank_item->attributes->setValue("data-location", $key);
            //$path_blank_item->attributes->setValue("data-post_id", $postID);
            $$key->addElement($node_container);
            //$$key->addElement($path_blank_item);

            $path_nodes->addElement($$key);
        }




        $path->addElement($path_nodes);

        echo $path->render();
    }

    public function selectNodeType($postID, $attr = array()){

             $form = new Element("div", new AttributeList(array("class" => "node_type_form")));
             $form_container = new Element("div", new AttributeList(array("class" => "form_container", "post-id" => $postID)));
             $form->addElement($form_container);

             $form_fields = new Row();


                  $form_fields->addElement(
                            InputExtentions::SegMdlDropDownList(
                                'path-node-type',
                                null,
                                null,
                                array('unknown'=>'-','requirement'=>'Requirement', 'collection_course'=>'Course Collection', 'single_course' => 'Single Course', 'information' => 'Information', 'action' => 'Call To Action'),
                                'notempty',
                                false
                            )
                        );

                    $form_container->addElement($form_fields);

             return $form;
    }

    public function newPathNodeHtml($nodeType, $location ,$post_id , $attr = array())
    {

        $newNode = new Element("div", new AttributeList($attr));
        $new_node_input = new Element('input', new AttributeList(array('type'=>'hidden','id' => 'new_node_field', 'name' => 'new_node_field[]', 'class' => 'new_node_input_field')));
       // $new_node_input_post_id = new Element('input', new AttributeList(array('value'=> $post_id,'id' => 'new_node_field', 'name' => 'new_node_field[]', 'class' => 'new_node_input_field')));
        if($nodeType === 'requirement'){

            $dropZone = new Element("div", new AttributeList(array(
                "class" => "new_node_drop_zone",
                "data-location"=> $location,
                "data-post_id" => $post_id,
                "ondragover" => "onDragOver(event)",
                "ondrop" => "onDrop(event)",
            )));
            $dropZone->addElement('Drag content here');
            
            

            $newNode->addElement($dropZone);
            $newNode->addElement($new_node_input);
          //  $newNode->addElement($new_node_input_post_id);
          


            echo $newNode->render();
        }

        if ($nodeType === 'collection_course') {
            $newNode->addElement($nodeType);
            echo $newNode->render();
        }

        if ($nodeType === 'single_course') {
            $newNode->addElement($nodeType);
            echo $newNode->render();
        }

        if ($nodeType === 'information') {



            $newNode->addElement($nodeType);
            echo $newNode->render();
        }

        if ($nodeType === 'action') {
            $newNode->addElement($nodeType);
            echo $newNode->render();
        }

       
        

       
    }   


}