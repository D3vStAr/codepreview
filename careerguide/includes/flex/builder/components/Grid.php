<?php

class Row extends Element
{

    /**
     * Row constructor.
     */
    public function __construct()
    {
        parent::__construct("div");
        $this->attributes->setValue("class", "row");
    }

}

class ColumnDivision
{
    /**
     * The division of the row the column should take up when displayed on extra small view ports.
     * @var Int|null
     */
    public $xs;

    /**
     * The division of the row the column should take up when displayed on small view ports.
     * @var Int|null
     */
    public $sm;

    /**
     * The division of the row the column should take up when displayed on medium view ports.
     * @var Int|null
     */
    public $md;

    /**
     * The division of the row the column should take up when displayed on large view ports.
     * @var Int|null
     */
    public $lg;

    public function __construct($xs = null, $sm = null, $md = null, $lg = null)
    {
        $this->xs = $xs;
        $this->sm = $sm;
        $this->md = $md;
        $this->lg = $lg;
    }

    private function convertToClassName($size, $dimension)
    {
        $parts = array("col", $size);
        if ($dimension != null) {
            array_push($parts, $dimension);
        }
        return implode("-", $parts);
    }

    public function convertToClassList()
    {
        $klasses = array($this->convertToClassName("xs", $this->xs));
        if ($this->sm != null) {
            array_push($klasses, $this->convertToClassName("sm", $this->sm));
        }
        if ($this->md != null) {
            array_push($klasses, $this->convertToClassName("md", $this->md));
        }
        if ($this->lg != null) {
            array_push($klasses, $this->convertToClassName("lg", $this->lg));
        }
        return implode(' ', $klasses);
    }

}

class Column extends Element
{

    public $divisions;

    /**
     * Column constructor.
     * @param $divisions ColumnDivision The divisions for the Column.
     */
    public function __construct($divisions)
    {
        $this->divisions = $divisions;
        $attributes = new AttributeList();
        $attributes->setValue("class", $divisions->convertToClassList());
        parent::__construct("div", $attributes);
    }

}

class Box extends Element {

    public function __construct($attributes = null)
    {
        parent::__construct('div', $attributes);
    }

}