<?php

class PaddleboardingLoader extends Element
{

    public function __construct($attributes = null)
    {
        parent::__construct('div', $attributes);
        $this->setClass("paddleboarding-loader");
        $this->attributes->appendToValue("class", "fade-out");
        $src = get_theme_root_uri() . "/meridianadventures/images/icons/paddleboarding-loader.gif";
        $image = new ImageElement($src);
        $image2 = new ImageElement($src);
        $this->addElement($image);
        $this->addElement($image2);
    }


}

class InfiniteLoader extends Element
{

    public function __construct($attributes = null)
    {
        parent::__construct('div', $attributes);
        $this->setClass("infinity-loader-container");
        $svg = new Element('svg', new AttributeList(array('viewBox' => '-2000 -1000 4000 2000"')));
        $svg->setClass("infinity-loader");
        $svg->addElement(new Element("path", new AttributeList(array('id' => 'inf', 'd' => 'M354-354A500 500 0 1 1 354 354L-354-354A500 500 0 1 0-354 354z'))));
        $svg->addElement(new Element("use", new AttributeList(array('class' => 'loader-animator', 'xlink:href' => '#inf', 'stroke-dasharray' => '1570 5143', 'stroke-dashoffset' => '6713px'))));
        $this->addElement($svg);
    }


}
