<?php

class ImageElement extends Element
{

    /**
     * Media constructor.
     * @param $src string The path of the source the images
     * @param null $attributes AttributeList A list of the attributes to the element
     */
    public function __construct($src, $attributes = null)
    {
        parent::__construct("img", $attributes);
        $this->attributes->setValue("src", $src);
    }

}
