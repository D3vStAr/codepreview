<?php


class MdlTextField extends Element
{
    public function __construct($text, $name, $attributes = null, $inputAttributes = null)
    {
        parent::__construct("div", $attributes);
        $this->attributes->appendToValue("class", "seg-mdl-textfield");

        // Add the text field
        $fieldAttributes = !empty($inputAttributes) ? $inputAttributes : new AttributeList(array());
        $fieldAttributes->appendToValue('class', 'seg-mdl');
        $field = new TextInput("text", $name, $fieldAttributes);

        //Add label
        $label = new Element("label", new AttributeList(array('for' => $name)));
        $label->setContent($text);

        //Add validation container
        $validation = new Element("div", new AttributeList(array('class' => 'mdl-validation-error-msg')));

        $this->addElement($field);
        $this->addElement($label);
        $this->addElement($validation);
    }
}

class TextInput extends Element
{

    public function __construct($type, $name, $attributes = null)
    {
        parent::__construct("input", $attributes);
        $alias = strtolower(str_replace(' ', '-', $name));
        $this->attributes->setValue("id", "input-" . $alias);
        $this->attributes->setValue("name", $alias);
        $this->attributes->setValue("type", $type);
    }

}

class Checkbox extends Element
{

    public function __construct($text, $value, $name, $attributes = null, $inputAttributes = null)
    {
        parent::__construct("div", $attributes);
        $alias = 'checkbox-' . str_replace(' ', '-', strtolower($text));
        $this->setClass("seg-mdl-dot-checkbox");

        $input = new Element("input", $inputAttributes);
        $input->attributes->setValue('type', 'checkbox');
        $input->attributes->setValue('id', $alias);
        $input->attributes->setValue('name', $name);
        $input->attributes->setValue('value', $value);
        $this->addElement($input);

        $label = new Element("label", new AttributeList());
        $dot = new Element('div');
        $label->attributes->setValue('for', $alias);
        $dot->setClass('seg-mdl-dot-checkbox-dot');
        $label->addElement($dot);
        $label->setContent($text);
        $this->addElement($label);
    }

}

class MDLSelect extends Element
{

    public function __construct($placeholder, $options, $name, $attributes = null, $inputAttributes = null)
    {
        parent::__construct("div", $attributes);
        $alias = 'select-' . str_replace(' ', '-', strtolower($name));
        $this->setClass("seg-mdl-dropdown");

        $label = new Element("label", new AttributeList());
        $label->attributes->setValue('for', $alias);
        $label->setContent($placeholder);
        $this->addElement($label);

        $select = new Element('select', $inputAttributes);
        $select->attributes->setValue('id', $alias);
        $select->attributes->setValue('name', $name);
        $this->addElement($select);

        foreach ($options as $oValue => $oName) {
            $option = new Element('option');
            $option->setContent($oName);
            $option->attributes->setValue('value', $oValue);
            $select->addElement($option);
        }

        $arrow = new Element('div');
        $arrow->setClass('seg-mdl-dropdown-arrow');
        $this->addElement($arrow);
    }

}

class Stepper extends Element
{

    public function __construct($text, $name, $attributes = null, $inputAttributes = null)
    {
        parent::__construct("div", $attributes);
        $this->attributes->appendToValue("class", "seg-mdl-counter-box");
        $inputs = new Element("div", new AttributeList(array('class' => 'inputs')));

        // Add label
        $label_wrapper = new Element("div", new AttributeList(array('class' => 'label')));
        $label = new Element("label", new AttributeList(array('for' => $name)));
        $label->setContent($text);

        // Add description
        $description = new Element('div', new AttributeList(array('class' => 'desc')));
        $label_wrapper->addElement($label);
        $label_wrapper->addElement($description);

        // Add the decrement counter
        $decrement = new Element("div", new AttributeList(array('class' => 'count-dec', 'anim' => 'ripple', 'data-id' => $name)));
        $inputs->addElement($decrement);

        // Add the text field
        $fieldAttributes = !empty($inputAttributes) ? ((is_array($inputAttributes) ? new AttributeList($inputAttributes) : $inputAttributes)) : new AttributeList(array());
        $fieldAttributes->appendToValue('class', 'seg-mdl');
        $field = new TextInput("text", $name, $fieldAttributes);
        $inputs->addElement($field);

        // Add the increment counter
        $increment = new Element("div", new AttributeList(array('class' => 'count-inc', 'anim' => 'ripple', 'data-id' => $name)));
        $inputs->addElement($increment);

        // Add direct children
        $this->addElement($label_wrapper);
        $this->addElement($inputs);
    }

}
