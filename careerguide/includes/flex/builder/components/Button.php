<?php

class Button extends Element
{

    /**
     * Button constructor.
     * @param $name string The name of the button, this will be used as the text of the button.
     * @param $isSubmitButton bool If true, this return element will be an input of type submit otherwise a button
     * element will be returned
     * @param $attributes AttributeList
     */
    public function __construct($name, $isSubmitButton = false, $attributes = null)
    {
        parent::__construct('button', $attributes);
        $alias = strtolower(str_replace(' ', '-', $name));
        $this->attributes->setValue("id", "btn-" . $alias);
        $this->attributes->setValue("anim", "ripple"); // Animation supported by SEG MDL Library.
        $this->attributes->setValue("name", $alias);
        $this->attributes->setValue("title", $name);
        if ($isSubmitButton) {
            $this->attributes->setValue("type", "submit");
        }
        parent::setContent($name);
    }

}

class ContainedButton extends Button
{
    /**
     * ContainedButton constructor.
     * @param $name string The name of the button, this will be used as the text of the button.
     * @param $isSubmitButton bool If true, this return element will be an input of type submit otherwise a button
     * element will be returned.
     * @param $attributes AttributeList
     */
    public function __construct($name, $isSubmitButton = false, $attributes = null)
    {
        parent::__construct($name, $isSubmitButton, $attributes);
        $this->attributes->appendToValue("class", "button");
    }

}

class OutlineButton extends Button
{
    /**
     * OutlineButton constructor.
     * @param $name string The name of the button, this will be used as the text of the button.
     * @param $isSubmitButton bool If true, this return element will be an input of type submit otherwise a button
     * element will be returned
     * @param $attributes AttributeList
     */
    public function __construct($name, $isSubmitButton = false, $attributes = null)
    {
        parent::__construct($name, $isSubmitButton, $attributes);
        $this->attributes->appendToValue("class", "button");
    }

}

class TextButton extends Button
{
    /**
     * TextButton constructor.
     * @param $name string The name of the button, this will be used as the text of the button.
     * @param $isSubmitButton bool If true, this return element will be an input of type submit otherwise a button
     * element will be returned
     * @param $attributes AttributeList
     */
    public function __construct($name, $isSubmitButton = false, $attributes = null)
    {
        parent::__construct($name, $isSubmitButton, $attributes);
        $this->attributes->appendToValue("class", "text");
    }

}