<?php

class Paragraph extends Element
{

    public function __construct($content, $attributes = null)
    {
        parent::__construct("p", $attributes);
        $this->setContent($content);
    }

}

class LinkElement extends Element
{

    public function __construct($anchorText, $link, $attributes = null)
    {
        parent::__construct("a", $attributes);
        $this->setContent($anchorText);
        $this->attributes->setValue("href", $link);
    }

}