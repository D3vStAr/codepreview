<?php

class Element
{

    public $tag;

    /**
     * @var AttributeList
     */
    public $attributes;

    /**
     * @var array<Element|String>
     */
    private $elements;

    public function __construct($tag, $attributes = null)
    {
        $this->tag = $tag;
        $this->attributes = isset($attributes) ? (is_array($attributes) ? new AttributeList($attributes) : $attributes) : new AttributeList();
        $this->elements = array();
    }

    /**
     * @param $element Element|String The element to add as a child of this element
     * @return Element The instance
     */
    public function addElement($element)
    {
        array_push($this->elements, $element);
        return $this;
    }

    /**
     * @param $content string A string contained in the element.
     * @return Element The instance
     */
    public function setContent($content)
    {
        array_push($this->elements, $content);
        return $this;
    }

    /**
     * Sets the class of the element
     * @param $klass string|array The string to set the class to
     */
    public function setClass($klass)
    {
        if (is_a($klass, 'array')) {
            $this->attributes->setValue("class", "");
            foreach ($klass as $k) {
                $this->attributes->appendToValue("class", $k);
            }
        } else {
            $this->attributes->setValue("class", $klass);
        }
    }

    /**
     * @return String|null return the html string of the element if `$isEchoed` is true else null
     */
    public function render()
    {
        $html = '<' . $this->tag . ' ' . $this->attributes->toString() . '>';
        foreach ($this->elements as $element) {
            if (is_a($element, "Element")) {
                $html .= $element->render();
            } else {
                $html .= $element;
            }
        }
        $html .= '</' . $this->tag . '>';
        return $html;
    }

}

class AttributeList
{

    public $list;

    public function __construct($list = array())
    {
        $this->list = $list;
    }

    /**
     * Appends a value to an attribute
     * @param $name string The name of the attribute to append to.
     * @param $value string The value that should be appended to the attribute.
     */
    public function appendToValue($name, $value)
    {
        $oldValue = (!empty($this->list[$name])) ? $this->list[$name] : '';
        $this->list[$name] = implode(' ', array($oldValue, $value));
    }

    /**
     * Sets the value to an attribute
     * @param $name string The name of the attribute.
     * @param $value string The value the attribute is set to.
     */
    public function setValue($name, $value)
    {
        $this->list[$name] = $value;
    }

    /**
     * Maps the attribute list to a string of space separated pairs of {$key="$value"}
     * @return string The mapped string
     */
    public function toString()
    {
        $values = array();
        foreach (array_keys($this->list) as $key) {
            if ($key == "href" && empty($this->list[$key])) {
                continue;
            } else {
                array_push($values, $key . '="' . trim($this->list[$key]) . '"');
            }
        }
        return implode(" ", $values);
    }

}
