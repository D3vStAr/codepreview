<?php

require_once "components/Components.php";

class ComponentRenderer
{

    public function containedButton($tag, $text, $attributes = array(), $isRounded = false, $isEchoed = true)
    {
        $classList = array("button ");
        $attributeList = array();
        if ($isRounded) {
            array_push($classList, "__rounded");
        }
        foreach (array_keys($attributes) as $key) {

            if ($key == "class") {
                array_push($classList, $attributes[$key]);
            } else {
                array_push($attributeList, $key . "=" . $attributes[$key]);
            }
        }
        $html = '';
        $html .= '<' . $tag . ' class="' . implode(" ", $classList) . '" ' . implode(" ", $attributeList) . '>' . $text . '</' . $tag . '>';
        if (!$isEchoed) {
            return $html;
        } else {
            echo $html;
        }
    }

    public function infinityLoader()
    {
        $html = '';
        $html .= '<div class="infinity-loader-container fade-in">';
        $html .= '<svg class="infinity0 500 0 1 0-354 354z"><-loader" viewBox="-2000 -1000 4000 2000">';
        $html .= '<path id="inf" d="M354-354A500 500 0 1 1 354 354L-354-354A50/path>';
        $html .= '<use class="loader-animator" xlink:href="#inf" stroke-dasharray="1570 5143" stroke-dashoffset="6713px"></use>';
        $html .= '</svg>';
        $html .= '</div>';
        echo $html;
    }

    /**
     * @param $tag String The html tag the created element should be
     * @return Element The newly created element
     */
    public function createElement($tag)
    {
        return new Element($tag);
    }

}