<?php

class FlexPathResolver
{

    public function urlWithPath($path)
    {
        return get_site_url(null, $path);
    }

    public function themeImageForName($name, $folder)
    {
        $paths = array(get_template_directory_uri(), "images");
        if ($folder) {
            array_push($paths, trim($folder, "/"));
        }
        array_push($paths, trim($name, "/"));
        return implode("/", $paths);
    }

    public function uploadedImageForName($name, $folder)
    {
        $paths = array(get_site_url(null, "wp-content/uploads"));
        if ($folder) {
            array_push($paths, trim($folder, "/"));
        }
        array_push($paths, trim($name, "/"));
        return implode("/", $paths);
    }

    public function theme_url()
    {
        $paths = array(get_site_url(null, "wp-content/themes/" . get_template()));
        return implode("/", $paths);
    }

}
