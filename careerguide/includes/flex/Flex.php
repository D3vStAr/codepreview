<?php

require_once   get_stylesheet_directory() . '/includes/flex/builder/HTMLHelper.php';
require_once   get_stylesheet_directory() . '/includes/flex/builder/ComponentRenderer.php';
require_once   get_stylesheet_directory() . '/includes/flex/helper/PathResolver.php';

class FlexComponentBuilder
{
    // Init vars
    public $html;
    public $cgpath;
    public $component;

    public function __construct()
    {   
        $this->html = new HTMLHelper();
   
        $this->component = new ComponentRenderer();
    }

}

class Flex
{

    // Init vars
    /** @var HTMLHelper */
    public $builder;
    public $path;

    public function __construct()
    {
        $this->builder = new FlexComponentBuilder();
        $this->path = new FlexPathResolver();
    }

    public function siteUrlWithPath($path)
    {
        return get_site_url(null, $path);
    }


}
