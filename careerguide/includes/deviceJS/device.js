    
window.tabletBreakpoint = 1070;
window.mobileBreakpoint = 767;
    
    
    $(window).on('resize', function (event) {
        var width = $(window).outerWidth(true);
        var body = $(document).find("body").first();
        if (width <= window.mobileBreakpoint) {
            //mobile
            $(body).removeClass("device-desktop");
            $(body).removeClass("device-tablet");

            $(body).addClass("device-mobile");


            
        } else if (width <= window.tabletBreakpoint) {
            //tablet
            $(body).removeClass("device-mobile");
            $(body).removeClass("device-desktop");

            $(body).addClass("device-tablet");
        } else {
            //destop
            $(body).removeClass("device-mobile");
            $(body).removeClass("device-tablet");

            $(body).addClass("device-desktop");
        }
    });

    $(document).ready(function() {

    $(window).trigger("resize");


    });