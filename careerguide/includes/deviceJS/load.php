<?php



function register_deviveJS_styles()
{
    wp_register_style('deviveJS-style', get_stylesheet_directory_uri() . '/includes/deviceJS/device.css');
    wp_enqueue_style('deviveJS-style');
}
// Register style sheet.
add_action('wp_enqueue_scripts', 'register_deviveJS_styles');

function register_deviveJS_scripts()
{
    wp_register_script('deviveJS-script', get_stylesheet_directory_uri() . '/includes/deviceJS/device.js');
    wp_enqueue_script('deviveJS-script');
}
// Register style sheet.
add_action('wp_enqueue_scripts', 'register_deviveJS_scripts');