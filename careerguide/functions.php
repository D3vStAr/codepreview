<?php


show_admin_bar(false);
/* 
* Creates Test Pages to Use as Sample IN Navs and Menu
*/
function wp_get_pages()
{
    $navItems = array('Deck', 'Interior', 'Engineering', 'Seminars', 'Events');
    return $navItems;
}


/* Flex Toolkit
 -----------------------------------------------------------------------*/

/**
 * @return string The path to the flex include file.
 */
function get_flex_file()
{
    return get_stylesheet_directory() . '/includes/flex/Flex.php';    
}

/**
 * @return Flex An instance of the flex toolkit.
 */
function get_flex_library()
{
    return new Flex();
}

require_once get_flex_file();
$flex = get_flex_library();


function register_scripts()
{

    /* jQuery - https://code.jquery.com*/
    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', false, '3.5.1');
    wp_enqueue_script('jquery');
}
// Register style sheet.
add_action('wp_enqueue_scripts', 'register_scripts');


//SEG Scripts 
// Loads SEG MDL Library for forn inputs
require_once get_stylesheet_directory() . '/includes/mdl/load.php';

//SEG Light Box
require_once get_stylesheet_directory() . '/includes/lightbox/load.php';


//SEG DeviceJS
/*
 *  Detects  Screen Width and Applies Correct Tage 
 */
require_once get_stylesheet_directory() . '/includes/deviceJS/load.php';


//SEG StickyJS
require_once get_stylesheet_directory() . '/includes/stickyJS/load.php';



/* Project Spesific Imports */
// Loads Path Class
require_once get_stylesheet_directory() . '/includes/segca-paths/load.php';
// Loads Courses Class
require_once get_stylesheet_directory() . '/includes/segca-courses/load.php';




/**
 * Registers a stylesheet.
 */
function register_styles()
{
    wp_register_style('style', get_stylesheet_directory_uri() . '/style.css');
    wp_enqueue_style('style');
}
// Register style sheet.
add_action('wp_enqueue_scripts', 'register_styles');



/**
 * Registers a admin stylesheet.
 */
function register_admin_styles()
{
    wp_register_style('style-admin', get_stylesheet_directory_uri() . '/admin-style.css');
    wp_enqueue_style('style-admin');
}
// Register style sheet.
add_action('admin_enqueue_scripts', 'register_admin_styles');


//Register Menu Positions
function theme_setup()
{
    register_nav_menus(
        array(
            'main-menu' => __('Main Menu'),
            'footer-menu' => __('Footer Menu'),
            'career-paths' => __('Paths Menu'),            
            'social-media' => __('Social Media Icons'),
            'payment-methods' => __('Accepted Payment Methods'),
            'footer-links' => __('Footer Links')
        )
    );
}
add_action('after_setup_theme', 'theme_setup');


//Custom Login Sceen Styles
function my_login_stylesheet()
{
    wp_enqueue_style('custom-login', get_stylesheet_directory_uri() . '/style-login.css');
    wp_enqueue_script('custom-login', get_stylesheet_directory_uri() . '/style-login.js');
}
add_action('login_enqueue_scripts', 'my_login_stylesheet');


//Wordpress Spesific Helper Functions

function segma_post_category($post_id){
    $category = get_the_category($post_id);
    return $category;
}

function segma_post_title($post_id)
{
    $title = get_the_title($post_id);
   return $title;
}

function get_custom_posts($postType)
{
    $args = array(
        'post_type' => $postType,
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC',
    );

    return new WP_Query($args); 
}

