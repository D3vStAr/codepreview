<?php require_once get_flex_file();
        $flex = get_flex_library(); ?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <?php wp_head(); ?>
</head>
<body>
    <header class="container-fluid">
        <?php //$flex->builder->html->renderBreadCrumbs('interior'); ?>
    </header>
    <div id="navbar" class="container-fluid">
        <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ));?>       
    </div>