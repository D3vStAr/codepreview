<?php get_header(); ?>
<section class="container">    
<?php

    $path_items = get_post_meta($post->ID, '_wporg_meta_key', true);
    $flex->builder->html->pathFrontEndBuilder($post->ID, $path_items, array('id' => 'path_wrapper', 'class' => 'path'));
    
?>
</section>
<?php get_footer(); ?>