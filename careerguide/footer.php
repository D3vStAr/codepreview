<?php require_once get_flex_file();
$flex = get_flex_library(); ?>
<footer class="container-fluid">
    <section class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="footer-inner-about">
                    <div class="footer-inner-title">About Ephemeris</div>
                    <div class="footer-inner-content">
                        <p>Possible routes and courses to take when you decide you want to start in the industry. This is a comprehensive and succinct guide.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <div class="footer-inner-paths">
                    <div class="footer-inner-title">Career paths</div>
                    <div class="footer-inner-content">
                        
                        <?php wp_nav_menu(array('theme_location' => 'career-paths')); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-2">
                <div class="footer-inner-nav">
                    <div class="footer-inner-title">Navigation</div>
                    <div class="footer-inner-content">

                        <?php wp_nav_menu(array('theme_location' => 'footer-menu')); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5">
                <div class="footer-inner-newsletter">
                    <div class="footer-inner-title">Subscribe To Our Newsletter To Receive The Latest News</div>
                    <div class="footer-inner-content">
                        <?php $flex->builder->html->renderSubscribe(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">

                <?php //wp_nav_menu(array('theme_location' => 'social-media')); ?>
            </div>
        </div>

        <div class="row footer-copy-notice">
            <div class="col-xs-10">
                <p><small>Website created and managed by SEG Management Applications & Solutions</p></small>
            </div>
            <div class="col-xs-2">

                <?php //wp_nav_menu(array('theme_location' => 'payment-methods')); ?>
            </div>
        </div>
    </section>
</footer>
</body>

</html>